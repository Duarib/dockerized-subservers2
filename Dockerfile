FROM java:8
WORKDIR /SubServers2
RUN apt-get install -y wget
RUN wget https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/bootstrap/target/BungeeCord.jar
RUN wget https://dev.me1312.net/jenkins/job/SubServers%20Platform/lastSuccessfulBuild/artifact/Artifacts/SubServers.Bungee.jar
ENTRYPOINT ["java", "-jar", "SubServers.Bungee.jar"]
